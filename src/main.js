import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

require('@/materialize/css/materialize.min.css');
require('@/sass/base.sass');
require('@/material-icons/material-icons.css');
require('@/materialize/js/materialize.min.js');
require('./typescript/main.js');

(() => {
    window.carritoUsuario = [];
    window.usuarioActual = [
        {
            nombre: 'Usuario',
            email: 'ejemplo@l-assiete.com',
            historial: {},
            logged: false
        }
    ];
    window.elRouter = router
})();

new Vue({
  data: {
    carritoUsuario: []
  },
  router,
  render: h => h(App)
}).$mount('#app')
