import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/platos/',
            name: 'Platos',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ './views/Platos.vue')
        },
        {
            path: '/carrito/',
            name: 'Carrito',
            component: () => import('./views/carrito')
        },
        {
            path: '/iniciar-sesion/',
            name: 'IniciarSesion',
            component: () => import('./views/Login-Registro')
        },
        {
            path: '/mi-cuenta/',
            name: 'MiCuenta',
            component: () => import('./views/MiCuenta')
        },
        {
            path: '/mi-pedido/',
            name: 'MiPedido',
            component: () => import('./views/MiPedido')
        },
        {
            path: '/a/',
            name: 'Administracion',
            component: () => import('./views/Administracion/Inicio'),
            children: [
                { path: 'usuarios', component: () => import('./components/Admin/Usuarios') },
                { path: 'empleados', component: () => import('./components/Admin/Empleados') }
            ]
        }
    ]
})
