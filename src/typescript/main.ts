function onSignIn(googleUser:any) {
    console.log("Mirame we, estoy iniciando sesion v:");
    let id_token = googleUser.getAuthResponse().id_token;

    let xhr = new XMLHttpRequest();
    xhr.open('POST', 'https://yourbackend.example.com/tokensignin');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
        console.log('Signed in as: ' + xhr.responseText);
    };
    xhr.send('idtoken=' + id_token);
}

// @ts-ignore
window.irAlCarro = () => {
    // @ts-ignore
    window.elRouter.push('/carrito/');
};