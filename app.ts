require('@google-cloud/debug-agent').start();

const expressr = require('express');
const apuri = expressr();
const session = require('express-session');

apuri.use(expressr.json());
apuri.use(expressr.urlencoded());
apuri.use(expressr.static('dist'));
apuri.use(session({
    secret: 'azopotamadre we v:',
    resave: false,
    saveUninitialized: false
}));

/* PUERTO NECESARIO PARA DEPLOYAR EN GOOGLE CLOUD!!! */
const puerto = 8080;

apuri.post('/log-inG', require('./Backend/revisarUsuarioG.js').revisarUsuarioG);
apuri.post('/usuarios', require('./Backend/usuarios.js').usuarios);
apuri.post('/registro', require('./Backend/registrarUsuario.js').registrar);
apuri.post('/sesion', require('./Backend/usuarioLogueado').revisarSesion);
apuri.post('/pedidos/actual', require('./Backend/Pedidos/obtenerPedidosActuales').obtenerPedido);

apuri.get('/');


apuri.listen(puerto, () => console.log(`Estoy escuchando el puerto ${puerto} we v:`));