const exec = (nombre:string, email:string, pass:string, sesion:any) =>
    new Promise((resolve, reject) => {
        const mysql = require('mysql');
        const SQL_CONNECT_DATA = require('./VARIABLES').sqlConnectData;

        const con = mysql.createConnection(SQL_CONNECT_DATA);

        con.connect((err:any) => {
            if (err) reject("Error al conectarse a la BBDD. registrarUsuarios.ts 10.\n" + err);

            con.query(
                `SELECT email FROM usuarios WHERE email='${email}'`,
                (err:any, respuesta:any) => {
                    const data = JSON.stringify(respuesta);
                    if (data === "[]") {

                        con.query(
                            `INSERT INTO usuarios (GOOGLE_ID, nombre, email, img, carritoActual, contrasena, ROL)
                                    VALUES ('0','${nombre}', '${email}', '/img/usuario.png', '{}', '${pass}', 'user' )`,
                            (err:any, respuesta:any) => {
                                if (err) reject ('Error al ejecutar SQL. registrarUsuario.ts 21\n' + err);

                                /* El servidor me obligo a hacer esto en vez de usar SELECT LAST_INSERT_ID() */
                                con.query(
                                    `SELECT USER_ID FROM usuarios WHERE email='${email}'`,
                                    (err:any, respuesta:any) => {
                                        if (err) reject ('Error al ejecutar SQL. registrarUsuario.ts 27\n' + err);

                                        /* Crea la variable de sesion */
                                        sesion.nombre = nombre;
                                        sesion.email = email;
                                        sesion.img = '/img/usuario.png';
                                        sesion.carritoActual = '{}';
                                        sesion.USER_ID = respuesta[0]["USER_ID"];
                                        sesion.ROL = 'user';

                                        sesion.save((err:any) => {
                                            if (err) console.log(`Error :c\n${err}`);
                                            console.log("En teoria ya guarde la sesion....");
                                        });

                                        const datos = {
                                            nombre,
                                            email,
                                            img: '/img/usuario.png',
                                            carritoActual: '{}',
                                            'USER_ID': respuesta[0]["USER_ID"],
                                            ROL: 'user',
                                            logged: true
                                        };
                                        resolve(`{ "existe": false, "creado": true, "data": ${JSON.stringify(datos)} }`);
                                    }
                                );
                            }
                        );

                    } else {
                        resolve(`{ "existe": true }`);
                    }
                }
            );

        });
    });

const registrar = (req:any, res:any) => {
    res.header('Access-Control-Allow-Origin', 'http://localhost:8081');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    const sesion = req.session;

    const nombre:string = req.body.nombre;
    const email:string = req.body.email;
    const pass:string = req.body.pass;

    if (nombre && email && pass) {
        exec(nombre, email, pass, sesion)
            .then(respuesta => {
                res.send(respuesta);
            })
            .catch(reason => console.log("Ups. Un error.\n" + reason));
    } else {
        res.send("Error, datos insuficientes.");
    }

};

module.exports.registrar = registrar;