function revisarSesion(req:any, res:any) {
    res.header('Access-Control-Allow-Origin', 'http://localhost:8081');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    const sesion = req.session;

    const USER_ID = sesion.USER_ID;
    if (USER_ID) {
        console.log("Recorde el usuario :3");

        const nombre = sesion.nombre;
        const email = sesion.email;
        const img = sesion.img;
        const carritoActual = sesion.carritoActual;
        const ROL = sesion.ROL;

        const data = {
            nombre,
            email,
            img,
            carritoActual: JSON.parse(carritoActual),
            ROL,
            USER_ID,
            logged: true
        };

        res.send(JSON.stringify(data));
    } else {
        console.log("Hmmph, no hay nadie logeado....");
        res.send(`{ "logged": false }`);
    }
}

module.exports.revisarSesion = revisarSesion;