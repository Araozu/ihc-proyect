const obtenerPedidosActuales = (req:any, res:any) => {
    res.header('Access-Control-Allow-Origin', 'http://localhost:8081');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    const USER_ID = req.body.USER_ID;
    const storedUserId = req.session.USER_ID.toString();

    console.log(`La comparacion: ${USER_ID} === ${storedUserId}. ${typeof USER_ID} y ${typeof  storedUserId}`);

    if (USER_ID === storedUserId) {

        const SQL_CONNECT_DATA = require('../VARIABLES').sqlConnectData;
        const con = require('mysql').createConnection(SQL_CONNECT_DATA);

        con.connect((err:any) => {
            if (err) {
                console.log(`Error al conectarse a la base de datos en obtenerPedidosActuales.ts 13.\n${err}`);
                res.send(`{"error": "Error al conectarse a la base de datos."}`);
                return;
            }

            con.query(
                `SELECT BOLETA_ID, transacciones, horaEntrega FROM boletas WHERE USER_ID='${USER_ID}'
                        ORDER BY BOLETA_ID DESC `,
                (err:any, response:any) => {
                    if (err) {
                        console.log(`Error al obtener las boletas del usuario ${USER_ID} en obtenerPedidosActuales.ts \
                        23.\n${err}`);
                        res.send("");
                        return;
                    }

                    res.send(JSON.stringify(response));
                    res.end();
                }
            );

        });

    } else {
        res.send(`{"error": "El usuario que envia la peticion y el usuario logeado no coinciden."}`);
    }

};

module.exports.obtenerPedido = obtenerPedidosActuales;