/* Metodo que obtiene los usuarios que existen. */
const obtenerUsuarios = () => {
    return new Promise((resolve, reject) => {
        const con = require('mysql').createConnection({
            host: '35.193.16.64',
            user: 'Araozu',
            password: 'xsakah4b',
            database: 'Usuarios_DB'
        });

        con.connect( (err:any) => {
            if (err) {
                console.log("Error en usuarios.ts 13.\n" + err);
                reject("Error :c");
            }

            con.query(
                `SELECT USER_ID, GOOGLE_ID, nombre, ROL FROM usuarios WHERE ROL='user'`,
                (err:any, response:any) => {
                    if (err) {
                        console.log("Error en usuarios.ts 21.\n" + err);
                        reject("Error :c");
                    }
                    resolve(response);
                }
            );
        });
    });
};

const procesar = (req:any, res:any) => {
    res.header('Access-Control-Allow-Origin', 'http://localhost:8081');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    obtenerUsuarios()
        .then((resultado:object) => res.send(JSON.stringify(resultado)))
        .catch(reason => res.send(reason));
};

module.exports.usuarios = procesar;