declare var module:any;
declare var require:any;

const {OAuth2Client} = require('google-auth-library');
const CLIENT_ID:string = "443321715214-9joe4c9l00osc6qcc4l0i2k3420jvb7q.apps.googleusercontent.com";
const client = new OAuth2Client(CLIENT_ID);

async function verify(token:string) {
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: CLIENT_ID
    });
    return ticket.getPayload();
}

module.exports.validator = verify;