const validator = require('./google_validation.js').validator;

const crearUsuario = (con:any, data:object) =>
    new Promise((resolve, reject) => {
        con.query (
            `INSERT INTO usuarios (nombre, email, img, carritoActual, GOOGLE_ID, ROL)\ 
            VALUES ('${(data as any)["nombreUsuario"]}', '${(data as any)["emailUsuario"]}',
            '${(data as any)["imagenUsuario"]}', '{}', '${(data as any)["googleID"]}', 'user')`,
            (err:any, res:string) => {
                if (err) {
                    reject("Error al ejecutar query SQL (revisarUsuarioG.ts fila 11))...\n" + err);
                }

                /* Luego de insertar el usuario, como GET_LAST_INDEX no funciona, otra query obtiene el num de usuario */
                con.query(
                    `SELECT USER_ID FROM usuarios WHERE GOOGLE_ID='${(data as any)["googleID"]}'`,
                    (err:any, res:any) => {
                        if (err) reject(`Error al ejecutar query SQL en revisarUsuarios.ts 18.\n${err}`);

                        resolve(res[0]["USER_ID"]);
                    }
                );
            }
        );
    });

const revisarUsuarioG = (usuario:object, sesion:any) => {
    return new Promise((resolve, reject) => {
        const mysql = require('mysql');
        const SQL_CONNECT_DATA = require('./VARIABLES').sqlConnectData;

        const con = mysql.createConnection(SQL_CONNECT_DATA);

        con.connect( (err:any) => {
            if (err) {
                reject("Error al conectarse a la BBDD.");
            }

            const nombreUsuario:string = (usuario as any)["name"];
            const emailUsuario:string = (usuario as any)["email"];
            const imagenUsuario:string = (usuario as any)["picture"];
            const googleID:number = (usuario as any)["sub"];

            con.query (`SELECT nombre, email, img, carritoActual, USER_ID, ROL FROM usuarios WHERE GOOGLE_ID='${googleID}'`,
                (err:any, response:any) => {
                    if (err) {
                        reject("Error al ejecutar query SQL");
                    }

                    const respuestaBD:string = JSON.stringify(response);
                    if (respuestaBD === '[]') {
                        /* El usuario no existe :D */
                        crearUsuario(con, {nombreUsuario, emailUsuario, imagenUsuario, googleID})
                            .then( (res:any) => {

                                /* Almacenar la sesion :c */
                                sesion.nombre = nombreUsuario;
                                sesion.email = emailUsuario;
                                sesion.img = imagenUsuario;
                                sesion.USER_ID = res[0]["USER_ID"];
                                sesion.ROL = "user";
                                sesion.carritoActual = "{}";

                                sesion.save((err:any) => {
                                    if (err)
                                        console.log(`Error al guardar sesion :c\n${err}`)
                                });

                                resolve(`{ "nombre": "${nombreUsuario}", "email": "${emailUsuario}", "img": "${imagenUsuario}",
                                "carrito": {}, "rol": "user", "USER_ID": "${res}"}`);
                            })
                            .catch((msg:string) => {
                                reject("Error al crear el usuario...\n" + msg);
                            })
                    } else {
                        /* Almacenar la sesion :c */
                        sesion.nombre = nombreUsuario;
                        sesion.email = emailUsuario;
                        sesion.img = imagenUsuario;
                        sesion.USER_ID = response[0]["USER_ID"];
                        sesion.ROL = response[0]["ROL"];
                        sesion.carritoActual = response[0]["carritoActual"];

                        sesion.save((err:any) => {
                            if (err)
                                console.log(`Error :c\n${err}`)
                        });

                        console.log(`Respuesta servidor:\n${respuestaBD}`);

                        resolve(JSON.stringify(response[0]));
                    }
                });
        });

    });
};

const revisarUsuario = (req:any, res:any) => {
    res.header('Access-Control-Allow-Origin', 'http://localhost:8081');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    const token:string = req.body.idtoken;

    validator(token)
        .then( (usuario:object) => {
            revisarUsuarioG(usuario, req.session)
                .then(response => res.send(response.toString()))
                .catch(err => res.send(err))

        })
        .catch(console.error);

};

module.exports.revisarUsuarioG = revisarUsuario;